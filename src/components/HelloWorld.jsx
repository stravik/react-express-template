import React, { Component } from 'react';
import axios from 'axios';

export class HelloWorld extends Component {
    giveFreeRounds() {
        axios.post("/api/platform/action", { username: "WOW" })
            .then((response) => {
                console.log("Success", response);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    render() {
        return (
            <div className="hello-world">
                <h1>Hello World!</h1>
                <button onClick={this.giveFreeRounds}>
                    Give Free Rounds
                </button>
            </div>
        );
    }
}

export default HelloWorld;