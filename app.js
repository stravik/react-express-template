const express = require('express');
const webpack = require('webpack');
const bodyParser = require("body-parser");
const webpackMiddleware = require('webpack-dev-middleware');
const config = require('./webpack.config.js');
const app = express();
const routes = require("./routes");

app.set("json spaces", 2);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(webpackMiddleware(webpack(config), {
    publicPath: config.output.publicPath,
    contentBase: 'src',
    stats: {
        colors: true,
        hash: false,
        timings: true,
        chunks: false,
        chunkModules: false,
        modules: false
    }
}));

app.use("/api/platform", routes.platform);

app.get('*', function response(req, res) {
    res.end();
});

app.listen(3000, function(err) {
    console.info("Listening to port", 3000);
});
